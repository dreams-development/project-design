# Project design
## Common
- [Серьезное проектирование серьезных сайтов. Часть 1. Аналитика](https://seclgroup.ru/article_serious_design_of_the_serious_websites.html)
- [Серьезное проектирование серьезных сайтов. Часть 2. Визуализация](https://seclgroup.ru/article_serious_design_of_the_serious_websites_2.html)
- [Манипуляции пользователями сайта с помощью цветов](https://seclgroup.ru/article_manipulations_with_users_by_means_of_flowers.html)
- [Как мы делали стартап: с 0 до 1800 клиентов за 3 месяца](https://seclgroup.ru/article_as_we_did_a_startup.html)
- [Дизайн сайта, который генерирует большие продажи!](https://seclgroup.ru/articl_website_design_which_generates_big_sales.html)
- [26 советов начинающим стартаперам от старого предпринимателя](https://seclgroup.ru/article-26-startup-advices.html)
- [Принципы построения интерфейсов сайтов (UI)](https://seclgroup.ru/article-user-interface-architecture-principles.html)
- [Дизайн таблиц](https://habr.com/post/328220/)

## SEO
- [The Ultimate Guide to JavaScript SEO](https://www.elephate.com/blog/ultimate-guide-javascript-seo/)[[RUS part 1]](https://habr.com/company/ruvds/blog/350976/)
- [The Ultimate Guide to JavaScript SEO](https://www.elephate.com/blog/ultimate-guide-javascript-seo/), [[RUS part 2]](https://habr.com/company/ruvds/blog/351058/)
- [SEO в разработке eCommerce проектов (20 правил).](https://seclgroup.ru/article_seo_v_razrabotke_ecommerce_proyektov.html)
- [О чем должен помнить веб-разработчик, чтобы сделать всё по SEO-феншую](https://habr.com/post/417503/)


## Продающий дизайн интернет-магазина
- [Часть 1. Аналитика.](https://seclgroup.ru/article_prodayushchiy_dizayn_internet-magazina_analitika_1.html)
- [Часть 2. Элементы интерфейса.](https://seclgroup.ru/article_prodayushchiy_dizayn_internet-magazina_elementy_interfeysa_2.html)
- [Часть 3. Дизайн элементов.](https://seclgroup.ru/article_prodayushchiy_dizayn_internet-magazina_dizayn_elementov_3.html)

## Серьезное проектирование серьезного магазина.
- [Часть 1. Исследования](https://seclgroup.ru/article_serjoznoye_proyektirovaniye_serjoznogo_magazina_analitika.html)
- [Часть 2. Модули интернет-магазина](https://seclgroup.ru/article_serjoznoye_proyektirovaniye_serjoznogo_magazina_moduli_internet_magazina.html)
- [Часть 3. Карточка товара и не только](https://seclgroup.ru/article_serjoznoye_proyektirovaniye_serjoznogo_magazina_kartochka_tovara.html)
- [Часть 4. Субституты, комплементы, сравнение и другие инструменты увеличения конверсии.](https://seclgroup.ru/article_substituty_komplementy_sravneniye_drugiye_instrumenty_uvelicheniya_konversii.html)
- [Часть 5. Личный кабинет, корзина, доставка-оплата, рассылка и другое.](https://seclgroup.ru/article_proyektirovaniye_serjoznogo_magazin_lichnyy_kabinet_korzina_dostavka_oplata.html)
- [Часть 6. Мультибрендовость, персонализация, микроформаты, интеграция с 1С и торговыми площадками.](https://seclgroup.ru/article_proyektirovaniye_serjoznogo_magazin_multibrendovost_personalizatsiya_mikroformaty.html)